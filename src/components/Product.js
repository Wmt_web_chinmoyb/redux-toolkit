import React, { useState,useEffect } from 'react';
import {Row,Col} from "react-bootstrap"
import {add} from "../store/CartSlice";
import { useDispatch } from 'react-redux';


const Product = () => {
    const [products,setProducts]=useState([]);
    const dispatch=useDispatch();
    useEffect(() => {
      const fetchProduct=async()=>{
        let response=await fetch("https://fakestoreapi.com/products");
        let data=await response.json();
        setProducts(data)
      }
      fetchProduct()
    }, [])
    const handleClick=(product)=>{
      dispatch(add(product))
    }
 console.log(products)
  return (
    <div>
      <Row className='g-3'>
       {products.map((product)=>{
        return (
          <>
           <Col className='col-lg-3'>
            <div className='card-container '>
              <div className='w-100 h-25'>
              <img src={product.image} alt="pic" width={100} height={100} />
              </div>
              <div>
              <h5>{product.title}</h5>
              </div>
              <div>
                <button onClick={()=>handleClick(product)}>Add to cart</button>
              </div>
              
            </div>
           </Col>
           
          </>
        )
       })}
       </Row>
    </div>
  )
}

export default Product