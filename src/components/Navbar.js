import React from 'react'
import { Link } from 'react-router-dom'
import { useSelector } from 'react-redux'


const Navbar = () => {
  const item=useSelector(state=>state.cart)
  return (
    <div className='d-flex p-4 justify-content-between bg-black text-white '>
        <div>
        <span className='mx-5 fs-2'>Redux Store</span>
        <Link to="/" className='px-3'>Home</Link>
        <Link to="/cart">Cart</Link>
        </div>
        <div className='pt-3'>Cart items: {item.length}</div>
    </div>
  )
}

export default Navbar