import React from 'react'
import { useSelector,useDispatch } from 'react-redux'
import { remove } from '../store/CartSlice'
import {Row,Col} from  "react-bootstrap"

const Cart = () => {
  const product=useSelector(state=>state.cart)
  const removeDispatch=useDispatch()
  console.log(product)
  const handleClick=(id)=>{
      removeDispatch(remove(id))
  }
  return (
    <div>
      Cart
      <div>
      <div>
      <Row className='g-3'>
       {product.map((product)=>{
        return (
          <>
           <Col className='col-lg-3'>
            <div className='card-container '>
              <div className='w-100 h-25'>
              <img src={product.image} alt="pic" width={100} height={100} />
              </div>
              <div>
              <h5>{product.title}</h5>
              </div>
              <div>
                <button onClick={()=>handleClick(product.id)}>Remove from cart</button>
              </div>
              
            </div>
           </Col>
           
          </>
        )
       })}
       </Row>
    </div>

      </div>

       
    </div>
  )
}

export default Cart